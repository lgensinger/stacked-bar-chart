import { max } from "d3-array";
import { scaleBand, scaleLinear } from "d3-scale";
import { select } from "d3-selection";
import { transition } from "d3-transition";

import { ChartLabel, LinearGrid, StackLayout as SL } from "@lgv/visualization-chart";

import { configuration, configurationLayout, configurationSeries } from "../configuration.js";

/**
 * StackedBarChart is a series visualization.
 * @param {array} data - objects where each represents a series in the collection
 * @param {integer} height - artboard height
 * @param {integer} width - artboard width
 */
class StackedBarChart extends LinearGrid {
    constructor(data, width=configurationLayout.width, height=configurationLayout.height, StackLayout=null, seriesLabels=configurationSeries.useLabels, paddingSeries=configurationSeries.padding, normalize=configurationSeries.normalize, name=configuration.name, label=configuration.branding) {

        // initialize inheritance
        super(data, width, height, StackLayout ? StackLayout : new SL(data, { height: height, normalize: normalize, paddingSeries: paddingSeries, width: width }), label, name);

        // update self
        this.bar = null;
        this.barLabel = null;
        this.barLabelPartial = null;
        this.classBar = `${label}-bar`;
        this.classBarLabel = `${label}-bar-label`;
        this.classBarLabelPartial = `${label}-bar-label-partial`;
        this.classSeries = `${label}-series`;
        this.classSeriesLabel = `${label}-series-label`;
        this.series = null;
        this.seriesLabel = null;
        this.useSeriesLabels = seriesLabels;

    }

    /**
     * Construct a scale to separate bar values in a series.
     * @returns A d3 scale function.
     */
    get barScale() {

        let start = this.useSeriesLabels ? this.seriesLabelSpace : 0;
        let end = this.useSeriesLabels ? this.width - this.seriesLabelSpace : this.width;

        return scaleLinear()
            .domain([0, this.Data.normalize ? 1 : this.Data.seriesMax])
            .range([start, end]);
    }

    /**
     * Determine space between series and its label.
     * @returns A float representing the padding between label and form.
     */
    get seriesLabelSpace() {
        return this.seriesLabelMax + this.unit;
    }

    /**
     * Determine tallest series label value.
     * @returns A float representing the tallest series label letter.
     */
    get seriesLabelMax() {
        return max(this.Data.seriesLabels.map(k => this.Label.determineWidth(k)), d => d);
    }

    /**
     * Construct scale to separate series values.
     * @returns A d3 scale function.
     */
    get seriesScale() {
        return scaleBand()
            .domain(this.Data.seriesLabels)
            .rangeRound([0, this.height])
            .paddingInner(this.Data.padding);
    }

    /**
     * Determine bar height.
     * @param {d} object - d3.js stack object
     * @returns A float representing pixel location for height value.
     */
    barHeight(d) {
        return this.seriesScale.bandwidth();
    }

    /**
     * Determine bar width.
     * @param {d} object - d3.js stack object
     * @returns A float representing pixel location for height value.
     */
    barWidth(d) {
        return this.barScale(d[0][1] - d[0][0]) - this.barScale.range()[0];
    }

    /**
     * Determine bar label x.
     * @param {d} object - d3.js stack object
     * @returns A float representing pixel location for x value.
     */
    barX(d) {
        return this.barScale(d[0][0]);
    }

    /**
     * Determine bar y.
     * @param {d} object - d3.js stack object
     * @returns A float representing pixel location for y value.
     */
    barY(d) {
        return this.columnScale.range()[1] - this.columnScale(d[0][1]);
    }

    /**
     * Declare bar mouse events.
     */
    configureBarEvents() {
        this.bar
            .on("click", (e,d) => this.configureEvent("bar-click",d,e))
            .on("mouseover", (e,d) => {

                // update class
                select(e.target).attr("class", `${this.classBar} active`);

                // send event to parent
                this.configureEvent("bar-mouseover",d,e);

            })
            .on("mouseout", (e,d) => {

                // update class
                select(e.target).attr("class", this.classBar);

                // send event to parent
                this.artboard.dispatch("bar-mouseout", {
                    bubbles: true
                });

            });
    }

    /**
     * Position and minimally style label partials in SVG dom element.
     */
    configureBarLabelPartials() {
        this.barLabelPartial
            .transition().duration(300)
            .attr("class", this.classBarLabelPartial)
            .textTween((d,i) => this.tweenText(`${this.Data.extractLabel(d)}, `, this.Data.extractValue(d), this.barWidth(d) - (this.unit / 2), i));
    }

    /**
     * Position and minimally style bars in SVG dom element.
     */
    configureBarLabels() {
        this.barLabel
            .transition().duration(1000)
            .attr("class", this.classBarLabel)
            .attr("data-label", d => this.Data.extractLabel(d))
            .attr("x", d => this.barX(d))
            .attr("dx", "0.5em")
            .attr("y", d => this.seriesScale.bandwidth() * (this.unit / this.seriesScale.bandwidth()))
            .attr("pointer-events", "none");
    }

    /**
     * Position and minimally style bars in SVG dom element.
     */
    configureBars() {
        this.bar
            .transition().duration(1000)
            .attr("class", this.classBar)
            .attr("data-id", d => this.Data.extractId(d))
            .attr("data-label", d => this.Data.extractLabel(d))
            .attr("fill", "lightgrey")
            .attr("x", d => this.barX(d))
            .attr("y", 0)
            .attr("width", d => this.barWidth(d))
            .attr("height", d => this.barHeight(d));
    }

    /**
    * Position and minimally style bar group in svg dom.
    */
    configureSeries() {
        this.series
            .attr("class", this.classSeries)
            .attr("data-label", d => d.series)
            .attr("transform", d => `translate(0,${this.seriesScale(d.series)})`);
    }

    /**
    * Position and minimally style series label in svg dom.
    */
    configureSeriesLabels() {
        this.seriesLabel
            .transition().duration(1000)
            .attr("class", this.classSeriesLabel)
            .attr("data-label", d => d.key)
            .attr("x", 0)
            .attr("y", d => this.seriesScale.bandwidth() * (this.unit / this.seriesScale.bandwidth()))
            .attr("pointer-events", "none")
            .text(d => d);
    }

    /**
     * Construct bars in series in HTML DOM.
     * @param {node} domNode - HTML node
     * @returns A d3.js selection.
     */
    generateBars(domNode) {
        return domNode
            .selectAll(`.${this.classBar}`)
            .data(d => d.values)
            .join(
                enter => enter.append("rect"),
                update => update,
                exit => exit.transition()
                    .duration(1000)
                    .attr("transform", "scale(0)")
                    .remove()
            );
    }

    /**
     * Generate bar label partials in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateBarLabelPartials(domNode) {
        return domNode
            .selectAll(`.${this.classBarLabelPartial}`)
            .data(d => [d, d])
            .join(
                enter => enter.append("tspan"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Construct bar text in series in HTML DOM.
     * @param {node} domNode - HTML node
     * @returns A d3.js selection.
     */
    generateBarLabels(domNode) {
        return domNode
            .selectAll(`.${this.classBarLabel}`)
            .data(d => d.values)
            .join(
                enter => enter.append("text"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate visualization.
     */
    generateChart() {

        // initialize labeling
        this.Label = new ChartLabel(this.artboard, this.Data);

        // generate group for each series
        this.series = this.generateSeries(this.content);
        this.configureSeries();

        // generate bars in each series
        this.bar = this.generateBars(this.series);
        this.configureBars();
        this.configureBarEvents();

        // generate series labels
        this.seriesLabel = this.generateSeriesLabels(this.series);
        this.configureSeriesLabels();

        // generate bar labels in each series
        this.barLabel = this.generateBarLabels(this.series);
        this.configureBarLabels();

        // generate label partials so they tween separately
        this.barLabelPartial = this.generateBarLabelPartials(this.barLabel);
        this.configureBarLabelPartials();

    }

    /**
     * Construct bar group in HTML DOM.
     * @param {node} domNode - HTML node
     * @returns A d3.js selection.
     */
    generateSeries(domNode) {
        return domNode
            .selectAll(`.${this.classSeries}`)
            .data(this.data)
            .join(
                enter => enter.append("g"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Construct series text in HTML DOM.
     * @param {node} domNode - HTML node
     * @returns A d3.js selection.
     */
    generateSeriesLabels(domNode) {
        return domNode
            .selectAll(`.${this.classSeriesLabel}`)
            .data(d => this.useSeriesLabels ? [d.series] : [])
            .join(
                enter => enter.append("text"),
                update => update,
                exit => exit.remove()
            );
    }

};

export { StackedBarChart };
export default StackedBarChart;
